# Feedback Portal


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Java 8
Maven [3.3.9)

### Installing

* Clone project to your local machine using 'git clone https://zhyvotova@bitbucket.org/zhyvotova/feedback-portal.git' command
* Go to root project folder (feedback-portal)
* To run project execute 'mvn spring-boot:run'
* Open http://localhost:8080/

### Running project

* Open http://localhost:8080/
* Fill in registration form and then login using your creadentials.
* To leave feedback fill in feedback form and click 'Add feedback'
* To see your feedback click 'View my feedbacks'
* To logout and see all feedbacks open 'http://localhost:8080/' and click 'View all feedbacks'

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management