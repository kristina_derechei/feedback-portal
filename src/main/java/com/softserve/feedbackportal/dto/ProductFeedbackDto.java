package com.softserve.feedbackportal.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ProductFeedbackDto {

    @NotNull
    @NotEmpty
    private String productName;

    @NotNull
    @NotEmpty
    private String text;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
