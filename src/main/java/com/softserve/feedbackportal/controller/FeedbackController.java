package com.softserve.feedbackportal.controller;

import com.softserve.feedbackportal.dto.ProductFeedbackDto;
import com.softserve.feedbackportal.model.ProductFeedback;
import com.softserve.feedbackportal.model.User;
import com.softserve.feedbackportal.service.FeedbackService;
import com.softserve.feedbackportal.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class FeedbackController {

    private final FeedbackService feedbackService;
    private final UserService userService;

    public FeedbackController(FeedbackService feedbackService, UserService userService) {
        this.feedbackService = feedbackService;
        this.userService = userService;
    }

    @RequestMapping(value = "/feedback/all", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<ProductFeedback> feedbacks() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if ("anonymousUser".equals(authentication.getPrincipal().toString())) {
            return feedbackService.retrieveAll();
        } else {
            User user = userService.getByUsername(authentication.getPrincipal().toString());
            return feedbackService.retrieveByUserId(user.getId());
        }
    }

    @RequestMapping(value = "/feedback/add", method = RequestMethod.POST)
    public String saveFeedback(ProductFeedbackDto feedbackDto) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getByUsername(authentication.getPrincipal().toString());

        ProductFeedback productFeedback = new ProductFeedback(feedbackDto.getText(), feedbackDto.getProductName(), user);
        feedbackService.save(productFeedback);
        return "index";
    }
}
