package com.softserve.feedbackportal.controller;

import com.softserve.feedbackportal.dto.UserDto;
import com.softserve.feedbackportal.model.User;
import com.softserve.feedbackportal.service.UserService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    private final UserService userService;
    private final BCryptPasswordEncoder encoder;

    public UserController(UserService userService, UserDetailsService userDetailsService, BCryptPasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @RequestMapping(value = {"/user/registration", "/"}, method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new UserDto());
        return "registration";
    }

    @RequestMapping(value = "/user/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") UserDto userForm,
                               BindingResult bindingResult, Model model) {
        if (userService.getByUsername(userForm.getUsername()) != null) {
            return "registration";
        }
        User user = new User(userForm.getUsername(), encoder.encode(userForm.getPassword()));
        userService.save(user);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("userForm", new UserDto());
        return "login";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }
}
