package com.softserve.feedbackportal.service;

import com.softserve.feedbackportal.model.ProductFeedback;

import java.util.List;

public interface FeedbackService {

    Long save(ProductFeedback feedback);

    List<ProductFeedback> retrieveAll();

    List<ProductFeedback> retrieveByUserId(Long userId);
}
