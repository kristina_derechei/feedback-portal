package com.softserve.feedbackportal.service.impl;

import com.softserve.feedbackportal.model.ProductFeedback;
import com.softserve.feedbackportal.repository.FeedbackRepository;
import com.softserve.feedbackportal.service.FeedbackService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.apache.commons.lang3.Validate.notNull;

@Service
@Transactional(readOnly = true)
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    @Modifying
    @Transactional
    public Long save(ProductFeedback feedback) {
        ProductFeedback savedFeedback = feedbackRepository.save(notNull(feedback));
        return savedFeedback.getId();
    }

    @Override
    public List<ProductFeedback> retrieveAll() {
        return feedbackRepository.findAll();
    }

    @Override
    public List<ProductFeedback> retrieveByUserId(Long userId) {
        return feedbackRepository.findByUserId(notNull(userId));
    }
}
