package com.softserve.feedbackportal.service.impl;

import com.softserve.feedbackportal.model.User;
import com.softserve.feedbackportal.repository.UserRepository;
import com.softserve.feedbackportal.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Long save(User user) {
        userRepository.save(user);
        return user.getId();
    }

    @Override
    public User getByUsername(String userName) {
        return userRepository.findByUsername(userName);
    }
}
