package com.softserve.feedbackportal.service;

import com.softserve.feedbackportal.model.User;

public interface UserService {

    Long save(User user);

    User getByUsername(String userName);
}
