package com.softserve.feedbackportal.repository;

import com.softserve.feedbackportal.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
