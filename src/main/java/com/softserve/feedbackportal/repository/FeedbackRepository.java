package com.softserve.feedbackportal.repository;

import com.softserve.feedbackportal.model.ProductFeedback;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeedbackRepository extends JpaRepository<ProductFeedback, Long> {

    List<ProductFeedback> findByUserId(Long id);

}
