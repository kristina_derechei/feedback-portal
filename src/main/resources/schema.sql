drop table if exists user;
drop table if exists feedback;

create table user (
    user_id IDENTITY PRIMARY KEY,
    username VARCHAR(255),
    password VARCHAR(255),
);

create table feedback (
    feedback_id IDENTITY PRIMARY KEY,
    product_name VARCHAR(255),
    text VARCHAR(255),
    user_id INT,
    FOREIGN KEY(user_id) references user(user_id)
);