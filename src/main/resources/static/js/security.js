$(document).ready(function(){

    $("#loginform").submit(function(e) {
           e.preventDefault();
           var form = $(this);
           $.ajax({
                  type: "POST",
                  url: "/login",
                  data: form.serialize(),
                  success: function(data, textStatus, request) {
                      var fullToken = request.getResponseHeader('Authorization');
                      var token = fullToken.replace('Bearer ', '');
                      sessionStorage.setItem('token', token);
                      request.setRequestHeader("Authorization", "Bearer " +  token);
                      reloadIndex();
                  }
           });
       });

    function reloadIndex() {
        $.ajax({
                  type: "GET",
                  url: "/index",
                  beforeSend : function(xhr) {
                    var token = sessionStorage.getItem('token');
                    if (token) {
                      xhr.setRequestHeader("Authorization", "Bearer " +  token);
                    }
                  },
                  success: function(data) {
                        $('#loginform').hide();
                        $('#output').html(data);
                  }
             });
     }
    function sendRequestWithToken(url, method, data, successHandler) {
        $.ajax({
             type: method,
             url: url,
             data: data,
             beforeSend : function(xhr) {
               var token = sessionStorage.getItem('token');
               if (token) {
                 xhr.setRequestHeader("Authorization", "Bearer " +  token);
               }
             },
             success: successHandler
        });
    };

    function successHandler(data){
        alert(JSON.stringify(data));
    };

     $("#feedbacks").click(function(e) {
          e.preventDefault();
          sendRequestWithToken("/feedback/all", "GET", null, successHandler);
     });

     $("#addFeedbackForm").submit(function(e) {
          e.preventDefault();
          var form = $(this);
          sendRequestWithToken("/feedback/add", "POST", form.serialize(), reloadIndex);
     });
});
